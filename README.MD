# ARControls

ARControls is a library for iOS in Swift with some controls.


## Install


`pod 'ControlName', :git => 'https://gitlab.com/ArkitectureRepository/ARControls.git', :branch => 'ControlName'`


For a specific version use commits:


`pod 'ControlName', :git => 'https://gitlab.com/ArkitectureRepository/ARControls.git', :branch => 'ControlName', :commit => 'f397310a77543616bcb2378b145827e596f75079'`

## Controls

You can use the next controls (Redirection to the branch):

- [ARCalendar](https://gitlab.com/ArkitectureRepository/ARControls/tree/ARCalendar)
- [ARHourSelector](https://gitlab.com/ArkitectureRepository/ARControls/tree/ARHourSelector)
- [ARCheckButton](https://gitlab.com/ArkitectureRepository/ARControls/tree/ARCheckButton)
- [ARStep](https://gitlab.com/ArkitectureRepository/ARControls/tree/ARStep)
- [ARSegment](https://gitlab.com/ArkitectureRepository/ARControls/tree/ARSegment)
- [ARTextField](https://gitlab.com/ArkitectureRepository/ARControls/tree/ARTextField)

## LICENSE

You can use the controls on any project but please make a reference to the repository.

If you change any control code please make a Merge Request.

## AUTHOR

- Alvaro Royo
- [LinkedIn](https://www.linkedin.com/in/alvaroroyo/)
- alvaroroyo3@gmail.com
- [More projects](https://gitlab.com/alvaroroyo)
